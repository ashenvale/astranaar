<?php

/*
 * This file is part of the Ashenvale Astranaar package.
 * (c) Chrisyue <https://chrisyue.com/>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ashenvale\Astranaar\Test\Entity\Factory;

use Ashenvale\Astranaar\Entity\Factory\Exception\ViolationsException;
use Ashenvale\Astranaar\Entity\Factory\UserFactory;
use Ashenvale\Astranaar\Entity\User;
use Ashenvale\Astranaar\ValueObject\Registration;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserFactoryTest extends TestCase
{
    public function testCreate()
    {
        $username = 'admin';
        $password = 'admin';
        $encodedPassword = 'encodedPassword';

        $registration = new Registration($username, $password);

        $violations = $this->prophesize(ConstraintViolationListInterface::class);
        $violations->count()->shouldBeCalled()->willReturn(0);

        $validator = $this->prophesize(ValidatorInterface::class);
        $validator->validate($registration)->shouldBeCalled()
            ->willReturn($violations->reveal());

        $passwordEncoder = $this->prophesize(UserPasswordEncoderInterface::class);
        $passwordEncoder->encodePassword(new User($username), $password)
                        ->shouldBeCalled()
                        ->willReturn($encodedPassword);

        $factory = new UserFactory($validator->reveal(), $passwordEncoder->reveal());
        $user = new User($username);
        $user->setPassword($encodedPassword);
        $this->assertEquals($user, $factory->create($registration));
    }

    public function testCreateException()
    {
        $username = 'admin';
        $password = '';

        $registration = new Registration($username, $password);

        $violations = $this->prophesize(ConstraintViolationListInterface::class);
        $violations->count()->shouldBeCalled()->willReturn(1);

        $validator = $this->prophesize(ValidatorInterface::class);
        $validator->validate($registration)->shouldBeCalled()
            ->willReturn($violations->reveal());

        $passwordEncoder = $this->prophesize(UserPasswordEncoderInterface::class);
        $passwordEncoder->encodePassword()->shouldNotBeCalled();

        $this->expectException(ViolationsException::class);

        $factory = new UserFactory($validator->reveal(), $passwordEncoder->reveal());
        $factory->create($registration);
    }
}
