<?php

/*
 * This file is part of the Ashenvale Astranaar package.
 * (c) Chrisyue <https://chrisyue.com/>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ashenvale\Astranaar\Test\Manager;

use Ashenvale\Astranaar\Entity\Factory\UserFactory;
use Ashenvale\Astranaar\Entity\Repository\UserRepositoryInterface;
use Ashenvale\Astranaar\Entity\User;
use Ashenvale\Astranaar\Manager\UserManager;
use Ashenvale\Astranaar\ValueObject\Registration;
use PHPUnit\Framework\TestCase;

class UserManagerTest extends TestCase
{
    public function testRegister()
    {
        $username = 'admin';
        $password = 'admin';
        $encodedPassword = 'encodedPassword';

        $registration = new Registration($username, $password);
        $user = new User($username);
        $user->setPassword($encodedPassword);

        $factory = $this->prophesize(UserFactory::class);
        $factory->create($registration)->shouldBeCalled()->willReturn($user);

        $repository = $this->prophesize(UserRepositoryInterface::class);
        $repository->add($user)->shouldBeCalled();

        $manager = new UserManager($repository->reveal(), $factory->reveal());
        $manager->register($registration);
    }
}
