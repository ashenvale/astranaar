<?php

/*
 * This file is part of the Ashenvale Astranaar package.
 * (c) Chrisyue <https://chrisyue.com/>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ashenvale\Astranaar\Manager;

use Ashenvale\Astranaar\Entity\Factory\UserFactory;
use Ashenvale\Astranaar\Entity\Repository\UserRepositoryInterface;
use Ashenvale\Astranaar\Entity\User;
use Ashenvale\Astranaar\ValueObject\Registration;

class UserManager
{
    private $repository;

    private $factory;

    public function __construct(
        UserRepositoryInterface $repository,
        UserFactory $factory
    ) {
        $this->repository = $repository;
        $this->factory = $factory;
    }

    public function register(Registration $registration): User
    {
        $user = $this->factory->create($registration);
        $this->repository->add($user);

        return $user;
    }
}
