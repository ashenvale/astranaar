<?php

/*
 * This file is part of the Ashenvale Astranaar package.
 * (c) Chrisyue <https://chrisyue.com/>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ashenvale\Astranaar\ValueObject;

use Symfony\Component\Validator\Constraints as Assert;

class Registration
{
    /**
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * @Assert\NotBlank()
     */
    private $password;

    public function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
