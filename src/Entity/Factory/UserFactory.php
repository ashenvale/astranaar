<?php

/*
 * This file is part of the Ashenvale Astranaar package.
 * (c) Chrisyue <https://chrisyue.com/>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ashenvale\Astranaar\Entity\Factory;

use Ashenvale\Astranaar\Entity\Factory\Exception\ViolationsException;
use Ashenvale\Astranaar\Entity\User;
use Ashenvale\Astranaar\ValueObject\Registration;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserFactory
{
    private $validator;

    private $passwordEncoder;

    public function __construct(
        ValidatorInterface $validator,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $this->validator = $validator;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function create(Registration $registration): User
    {
        $violations = $this->validator->validate($registration);
        if (\count($violations) > 0) {
            throw new ViolationsException($violations);
        }

        $user = new User($registration->getUsername());

        $password = $this->passwordEncoder->encodePassword($user, $registration->getPassword());
        $user->setPassword($password);

        return $user;
    }
}
