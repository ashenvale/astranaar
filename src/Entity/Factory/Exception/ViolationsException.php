<?php

/*
 * This file is part of the Ashenvale Astranaar package.
 * (c) Chrisyue <https://chrisyue.com/>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ashenvale\Astranaar\Entity\Factory\Exception;

use Symfony\Component\Validator\ConstraintViolationListInterface;

class ViolationsException extends \RuntimeException
{
    private $violations;

    public function __construct(
        ConstraintViolationListInterface $violations,
        string $message = '',
        int $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }

    public function getViolations(): ConstraintViolationListInterface
    {
        return $this->violations;
    }
}
