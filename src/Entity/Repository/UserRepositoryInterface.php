<?php

/*
 * This file is part of the Ashenvale Astranaar package.
 * (c) Chrisyue <https://chrisyue.com/>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ashenvale\Astranaar\Entity\Repository;

use Ashenvale\Astranaar\Entity\User;
use Symfony\Component\Security\Core\User\UserProviderInterface;

interface UserRepositoryInterface extends UserProviderInterface
{
    public function add(User $user);
}
